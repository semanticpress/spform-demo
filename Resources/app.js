/*
 * This requires a form schema file in JSON format
 *
*/

// open a single window
var window = Ti.UI.createWindow({
	backgroundColor:'white'
});

var button = Ti.UI.createButton({
	height:44,
	width:200,
	title:'Open Form'
});

var form = require('sp.form');
form.load({
	schema:'form.schema.json',
	imageDir:'images/form/',
	geo:{lat:0,lng:0},
	save:function(data){
		Ti.API.info('Clicked save');
		alert('Saved!');
	},
	cancel: function() {}
});
	
button.addEventListener('click', function() {
	var v = form.createView({top:0,backgroundColor:'#fff'});
	var w = Ti.UI.createWindow({backgroundColor:'green'});
	w.add(v);
	
	var btnClose = Ti.UI.createButton({title:'Close'});
	var btnNext = Ti.UI.createButton({title:'Next'});
	btnClose.addEventListener('click',function(){
		w.close();
	});
	btnNext.addEventListener('click',function(){
		form.showSection(1);
	});
	w.setLeftNavButton(btnClose);
	w.setRightNavButton(btnNext);
	
	w.open({modal:true});
});

window.add(button);

window.open();
