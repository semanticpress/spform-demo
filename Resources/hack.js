/*
 * This is a hack solves the "Invalid method passed to UIModule" error
 * It works by forcing Titanium to load SDK components into memory.
 * 
 * Drop this file anywhere in your project and DON'T Ti.include() it.
 * Be sure this file extension is .js.
 * Clean and recompile your project.
 * 
 * Enjoy!
 * @tzmartin
 */

var hk1 = false;
hk1 = Ti.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, 'test.xml');
hk1 = Ti.UI.createAlertDialog();
hk1 = Ti.UI.createWindow();
hk1 = Titanium.UI.createButton();
hk1 = Titanium.UI.createLabel();
hk1 = Titanium.UI.createTextField();
hk1 = Titanium.UI.createTextArea();
hk1 = Titanium.UI.createImageView();
hk1 = Titanium.UI.createScrollView();
hk1 = Titanium.UI.createScrollableView();
hk1 = Titanium.UI.createPicker();
hk1 = Titanium.UI.createTableView();
hk1 = Titanium.UI.createToolbar();
hk1 = Titanium.UI.createProgressBar();
hk1 = Titanium.UI.createPicker();
hk1 = Titanium.UI.createPickerColumn();
hk1 = Titanium.Map.createView();