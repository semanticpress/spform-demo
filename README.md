# SPForm Demo

Demo App that uses the SPForm module.


![Screenshot]()


## Requirements

-   sp.form iOS / Android module

## License    

@authors	
		
-   Terry Martin <martin@semanticpress.com>

Copyright (c) 2011, Semantic Press, Inc. <http://www.semanticpress.com>
SPForm is Copyright (c) 2011 by Semantic Press, Inc. All Rights Reserved.
Titanium is Copyright (c) 2009-2010 by Appcelerator, Inc. All Rights Reserved.
Appcelerator, Appcelerator Titanium and associated marks and logos are 
trademarks of Appcelerator, Inc. 


@license    

Titanium is licensed under the Apache Public License (Version 2). Please
see the LICENSE file for the full license.

SPForm Module is licensed under the Apache Public License (Version 2). Please
see the LICENSE file for the full license.

